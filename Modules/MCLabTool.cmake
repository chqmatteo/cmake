macro(mclab_tool tool_name tool_version)

set(CMAKE_MACOSX_RPATH 1)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

cmake_policy(VERSION 3.3)

project(${tool_name} VERSION ${tool_version})

include(MCLabCommon)

file(GLOB libdirs "lib/*")

foreach(libdir ${libdirs})
  if(IS_DIRECTORY ${libdir})
    get_filename_component(library ${libdir} NAME)
    list(APPEND libraries ${library})
  endif(IS_DIRECTORY ${libdir})
endforeach(libdir ${libdirs})

set(MCLAB_LIB_BUILD_TYPE "Release" CACHE STRING "Build type for libraries")
set(CMAKE_BUILD_TYPE_SAVE "${CMAKE_BUILD_TYPE}")
set(CMAKE_BUILD_TYPE "${MCLAB_LIB_BUILD_TYPE}")
set(MCLAB_LIB_WARNINGS ON CACHE BOOL "Enable compiler warnings for libraries")
set(MCLAB_WARNINGS_SAVE "${MCLAB_WARNINGS}")
set(MCLAB_WARNINGS ${MCLAB_LIB_WARNINGS})
foreach(library ${libraries})
  add_subdirectory("lib/${library}" EXCLUDE_FROM_ALL)
endforeach(library ${libraries})
set(CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE_SAVE}")
set(MCLAB_WARNINGS "${MCLAB_WARNINGS_SAVE}")

endmacro()
