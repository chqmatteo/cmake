# Searches for an installation of the sundials library. On success, it sets the following variables:
#
#   SUNDIALS_FOUND              Set to true to indicate the sundials library was found
#   SUNDIALS_INCLUDE_DIR        The directory containing the header file sundials/sundials_config.h
#   SUNDIALS_LIBRARIES          The libraries needed to use the sundials library
#

find_path(SUNDIALS_INCLUDE_DIR NAMES sundials/sundials_config.h)
foreach(comp ${SUNDIALS_FIND_COMPONENTS})
  string(TOLOWER ${comp} comp_lowercase)
  find_library(SUNDIALS_${comp}_LIBRARIES NAMES sundials_${comp_lowercase})
  if(NOT (${SUNDIALS_${comp}_LIBRARIES} STREQUAL SUNDIALS_${comp}_LIBRARIES-NOTFOUND))
    list(APPEND SUNDIALS_LIBRARIES ${SUNDIALS_${comp}_LIBRARIES})
    set(SUNDIALS_${comp}_FOUND TRUE)
  endif()
endforeach()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(SUNDIALS HANDLE_COMPONENTS REQUIRED_VARS SUNDIALS_INCLUDE_DIR SUNDIALS_LIBRARIES)

mark_as_advanced(SUNDIALS_INCLUDE_DIR SUNDIALS_LIBRARIES)
