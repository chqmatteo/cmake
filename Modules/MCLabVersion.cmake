macro(mclab_version_parse version_string version_major version_minor version_patch)
  string(REGEX MATCHALL "[0-9]+" components "${version_string}")
  list(LENGTH components num)
  if(${num} GREATER 0)
    list(GET components 0 ${version_major})
    if(${num} GREATER 1)
      list(GET components 1 ${version_minor})
      if(${num} GREATER 2)
        list(GET components 2 ${version_patch})
      else()
        set(${version_patch} 0)
      endif()
    else()
      set(${version_minor} 0)
      set(${version_patch} 0)
    endif()
  else()
    set(${version_major} 0)
    set(${version_minor} 0)
    set(${version_patch} 0)
  endif()
endmacro()

macro(mclab_version_max v1 v2 result)
  mclab_version_parse("${v1}" v1_major v1_minor v1_patch)
  mclab_version_parse("${v2}" v2_major v2_minor v2_patch)
  if(${v1_major} EQUAL ${v2_major})
    if ("${v1_minor}.${v1_patch}" VERSION_GREATER "${v2_minor}.${v2_patch}")
      set(${result} ${v1})
    else()
      set(${result} ${v2})
    endif()
  else()
    set(${result} FALSE)
  endif()
endmacro()

macro(mclab_version_is_compatible vact vreq result)
  mclab_version_parse("${vact}" vact_major vact_minor vact_patch)
  mclab_version_parse("${vreq}" vreq_major vreq_minor vreq_patch)
  if(${vact_major} EQUAL ${vreq_major})
    if ("${vact_minor}.${vreq_patch}" VERSION_GREATER "${vreq_minor}.${vreq_patch}")
      set(${result} TRUE)
    elseif("${vact_minor}.${vreq_patch}" VERSION_EQUAL "${vreq_minor}.${vreq_patch}")
      set(${result} TRUE)
    else()
      set(${result} FALSE)
    endif()
  else()
    set(${result} FALSE)
  endif()
endmacro()
