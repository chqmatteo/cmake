macro(mclab_generate_config_header)
  configure_file(
    "${PROJECT_SOURCE_DIR}/config.h.in"
    "${PROJECT_BINARY_DIR}/config.h"
  )
endmacro(mclab_generate_config_header)
