======================
Infrastructure modules
======================
Modules that enable easy development of libraries and tools integrated into
our CMake infrastructure.
The primary modules that are intended to be included in your
``CMakeLists.txt`` files are:

* ``MCLabLibrary`` defining macro ``mclab_library(project_name project_version ...)``.

  This macro must be used in the beginning of ``CMakeLists.txt`` in the
  library projects.
  It automatically adds library target  with name ``project_name`` to be built
  from all the source files in `src` directory. Additional source files to
  be used can be specified.

  ``mclab_library_feature(feature_name)`` macro can be used to declare that
  library provides feature ``feature_name``.
  It is intended to be used in situations when some library functionality
  depends on the presence of some third-party libraries and you want to
  allow the user of your library to choose whether he needs such
  functionality.
  It can be done by declaring presence of the feature using this macro if
  corresponding third-party dependency is satisfied.
  Then user of your library that needs this feature can explicitly request
  it using ``mclab_link_library()`` macro described later.

  Note that we are using `semantic versioning <https://semver.org>`_ for the
  libraries.

  Have a look at `MCLabUtils <https://bitbucket.org/mclab/mclabutils>`_ repo
  for a good example of a library project.

  Library projects are not intended to be used as top level projects, they
  are supposed to be used as subprojects inside tool projects or a special
  project `devenv <https://bitbucket.org/mclab/devenv>`_ prepared to be used
  for libraries development.

* ``MCLabTool`` defining macro ``mclab_tool(project_name project_version)``.

  This macro must be used in the beginning of ``CMakeLists.txt`` in the tool
  projects.
  Differently from ``mclab_library()`` no targets are automatically added: it
  is very common in our setting that one tool project needs several executables
  (e.g. main tool, tool for postprocessing of experimental results, etc.)

  Another important functionality of this macro is making all MCLab libraries
  inside ``lib`` directory available to be used in the current project using
  ``mclab_link_library()`` macro described later.

  Other macros available in the tool project:
  
  - ``mclab_add_executable(name ...)``
    This macro adds target executable to be built from the list of
    provided source files.
  - ``mclab_check_deps()``
    This macro is intended to be called at the very end of ``CMakeLists.txt``
    file. It checks whether requested MCLab depedencies are satisfied.

  Here is the list of useful CMake cache variable available in the tool
  project:

  - ``MCLAB_WARNINGS`` boolean variable that allows toggling our very prudent
    compilation warnings.
  - ``MCLAB_LIB_WARNINGS`` boolean variable for toggling compilation warnings
    for the MCLab libraries used in this project.
  - ``MCLAB_LIB_BUILD_TYPE`` string variable defining build mode for MCLab
    libraries.
  - ``MCLAB_DEBUG_OPTIMIZATION_LEVEL`` defines the optimization level to use
    when compiling in Debug mode.
  - ``MCLAB_SANITIZER`` string variable that enables using compiler sanitizer
    feature. Its value is passed directly to ``-fsanitizer=`` compiler flag.

  Please have a look at
  `tool-template <https://bitbucket.org/mclab/tool-template.git>`_ repo
  containing full example of a tool project.

Here is the list of the macros that can be used in both types of the projects:

* ``mclab_find_and_link_library(target package_name [PKGCONFIG module0 [module1 ...]] ...)``
  Should be used to declare dependency on a third-party library.
  It automatically updates include directories and list of libraries to link
  with for the `target`.

  - Without ``PKGCONFIG`` part it wraps ``find_package()`` CMake functionality.
    All the arguments coming after package_name (e.g. ``VERSION``,
    ``REQUIRED``, etc) are passed to ``find_package()``.
  - When ``PKGCONFIG`` part is present it uses ``pkg_check_module()`` to find
    the requested package.

* ``mclab_link_library(target library_name VERSION version FEATURES ...)``
  Should be used to declare depedency on a MCLab library.
  
  - It is possible to specify the version of the requested library using
    ``VERSION`` one value keyword argument.
    Such requirement is interpreted as follows.
    Given version requirement ``MAJOR_REQ[.MINOR_REQ[.PATCH_REQ]]`` and the
    actual version ``MAJOR[.MINOR[.PATCH]]``, requirement is satisfied if
    ``MAJOR == MAJOR_REQ`` and ``[MINOR[.PATCH]] >= [MINOR_REQ[.PATCH_REQ]]``.
    If ``MINOR`` and/or ``PATCH`` are omitted they are considered to be equal to 0.
  - It is possible to specify list of features that requested library must
    provide using ``FEATURES`` multi value keyword argument.

* ``mclab_generate_config_header()``
  It wraps ``config_header()`` functionality of CMake.
  It expects ``config.h.in`` template in the top level of the project source
  directory and generates corresponding ``config.h`` header in the top level
  of the project binary directory.
  ``mclab_tool()`` and `mclab_library()` macros make this header "visible" to
  the project source files by adding project binary directory to the list
  of include directories.
* ``mclab_mpi(target ...)`` Enables compilation with MPI for the ``target``. Add REQUIRED to abort configuration if MPI is not available.
* ``mclab_openmp(target ...)`` Enables compilation with OpenMP for the ``target``. Add REQUIRED to abort configuration if OpenMP is not available.
* ``mclab_pthreads(target ...)`` Enables compilation with pthreads for the ``target``. Add REQUIRED to abort configuration if pthreads are not available.
* ``mclab_cxx_standard(target version)`` Sets C++ standard for the ``target``.
* ``mclab_python(target version)`` Enabled compilation linking to Python library. Add REQUIRED to abort configuration if Python is not available.

For both project types if there is ``Doxyfile.in`` and doxygen is present in
the system, build target ``doc`` is added.
For the libraries subprojects inside tool project target name is
consstructed as ``doc-library`` where ``library`` is the name of the library.

============
Find modules
============
Find modules for libraries that are not supported by CMake distribution.
List of libraries:

* `GSL <https://www.gnu.org/software/gsl/>`_
* `GMP <http://gmplib.org/>`_
* `GLPK <https://www.gnu.org/software/glpk/>`_
* `lp_solve <http://lpsolve.sourceforge.net/>`_
* `CPLEX <https://www-01.ibm.com/software/commerce/optimization/cplex-optimizer/>`_
* `PPL <http://bugseng.com/products/ppl/>`_
* `ZIP <https://libzip.org>`_
* `SQLite3 <http://sqlite.org>`_
* `MLPACK <http://mlpack.org>`_
* `ORTOOLS <https://developers.google.com/optimization/>`_
* `SUNDIALS <https://computation.llnl.gov/projects/sundials/>`_
